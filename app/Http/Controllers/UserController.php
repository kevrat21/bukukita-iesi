<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Buku;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function cekUser()
    {
        $email = $_POST['email'];
        $password = $_POST['password'];
        $user = user::where('email', $email)->first();
        if (!is_null($user)) {
            if ($user->password == $password) {
                Session::put('user_id', $user->id);
                return redirect('/');
            } else {
                return redirect('/login');
            }
        } else {
            return redirect('/login');
        }
    }

    public function logout()
    {
        Session::flush();
        return redirect('/');
    }

    // Session index admin belum jalan


    public function create(Request $request)
    {
        $inputUser = new User;

        $inputUser->name = $request->input('name');
        $inputUser->email = $request->input('email');
        $inputUser->password = $request->input('password');
        $inputUser->user_type = $request->input('user_type', 'user');

        $inputUser->save();
        return redirect('/login');
    }
}
