<?php

namespace App\Http\Controllers;

use App\Models\Buku;
use App\Models\Genre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $data = [
            'bukus' => Buku::all()
        ];
        if ($request != null) {
            $titleSearch = $request->input('search');
            $data['bukus'] = Buku::query()->where('title', 'LIKE', "%{$titleSearch}%")->get();
        }
        return view('pages.index', $data);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        if (Session::has('user_id')) {
            $data = [
                'bukus' => Buku::where('id', $id)->first()
            ];
            return view('/katalog', $data);
        }
        return redirect('/login');
    }

    public function detail($id)
    {

        $data = [
            'buku' => Buku::where('id', $id)->first()
        ];
        return view('pages.detail', $data);
    }
}
