@extends('layouts.app')

{{-- set title --}}
@section('title', 'BukuKita')

@section('content')

<section class="bg-primary">
    <div class="container">
        <div class="row pt-5">
            <h2 class="text-center text-white">Selamat Datang</h2>
        </div>
        <form class="form-inline" method="GET" action="/">
            <div class="input-group  pt-3 pb-5 center">
                <input type="text" class="form-control" placeholder="Cari buku disini..." name="search">
                <div class="input-group-append">
                    <button class="btn btn-secondary" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>

<section>
    <div class="container py-4 min-vh-100">
        <div class="row row-cols-1 row-cols-md-3 g-4" >
            @foreach ($bukus as $Buku)
            <div class="col" style="width: 15%">
                <div class="card h-100">
                  <img src="img/{{ $Buku->cover }}" class="card-img-top" height="80%" width="25%">
                  <div class="card-body">
                    <h5 class="card-title text-truncate">{{$Buku->title }}</h5>
                    <p class="card-text text-truncate">{{$Buku->synopsis}}</p>
                    <a href="/detail/{{ $Buku->id }}" class="btn btn-primary">Lihat</a>
                  </div>
                </div>
              </div>
            @endforeach
        </div>
    </div>
</section>

@endsection

@push('after-style')

@endpush
